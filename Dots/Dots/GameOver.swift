//
//  GameOver.swift
//  Dots
//
//  Created by Jacob Ruzi on 4/30/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

import Foundation
import SpriteKit

class GameOver: SKScene {
    var numDots = 0
    var score = 0
    
    let scoreTile = SKShapeNode()
    
    override func didMoveToView(view: SKView) {
        //Get the high score
        var highScore = NSKeyedUnarchiver.unarchiveObjectWithFile(GameData.ArchiveURL.path!) as? Int
        if highScore == nil {
            highScore = 0
        }
        if (score > highScore) {
            highScore = score
            NSKeyedArchiver.archiveRootObject(highScore!, toFile: GameData.ArchiveURL.path!)
        }
        print("the high score is " + String(highScore))
        
        self.backgroundColor = UIColor.whiteColor()
        
        //Game title label
        let myLabel = SKLabelNode(fontNamed:"SavoyeLetPlain")
        myLabel.text = "game over"
        myLabel.fontSize = 80
        myLabel.fontColor = UIColor.blackColor()
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:0.55 * self.frame.size.height)
        myLabel.zPosition = 2
        
        let scoreLabel = SKLabelNode(fontNamed:"SavoyeLetPlain")
        scoreLabel.text = "your score: " + String(score)
        scoreLabel.fontSize = 80
        scoreLabel.fontColor = UIColor.blackColor()
        scoreLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:myLabel.position.y - myLabel.frame.size.height - 8)
        scoreLabel.zPosition = 2
        
        scoreTile.alpha = 0.8
        scoreTile.path = CGPathCreateWithRoundedRect(CGRectMake(-150,0,300,120), 8, 8, nil)
        scoreTile.strokeColor = UIColor(red: 210/255.0, green: 215/255.0, blue: 211/255.0, alpha: 1.0)
        scoreTile.fillColor = scoreTile.strokeColor
        scoreTile.position = CGPoint(x:CGRectGetMidX(self.frame), y:(myLabel.position.y + scoreLabel.position.y) / 2 - scoreTile.frame.size.height / 2 + 10)
        scoreTile.zPosition = 1
        
        //Have dots floating around in background
        for _ in numDots...50 {
            addDot()
        }

        self.addChild(scoreTile)
        self.addChild(myLabel)
        self.addChild(scoreLabel)
    }
    
    /* Adds dots for asthetics */
    func addDot() {
        numDots = numDots+1
        
        let dot = SKShapeNode(circleOfRadius: 8)
        dot.fillColor = dotColors[Int(arc4random_uniform(UInt32(dotColors.count)))]
        dot.strokeColor = dot.fillColor
        dot.position = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        dot.zPosition = 0
        
        let destination = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        
        let moveTo = SKAction.moveTo(destination, duration: 3.0 + NSTimeInterval(arc4random_uniform(6)))
        let fadeOut = SKAction.fadeOutWithDuration(1.0)
        let decCount = SKAction.runBlock({ self.numDots = self.numDots - 1})
        let sequence = SKAction.sequence([moveTo,fadeOut,decCount])
        
        dot.runAction(sequence)
        
        self.addChild(dot)
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if numDots < 50 {
            addDot()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch ends */
            
        //Return to main menu
        //let reveal = SKTransition.fadeWithColor(UIColor.whiteColor(), duration: 1.0)
        //let nextScreen = GameScene(size: self.view!.bounds.size)
        //self.view!.presentScene(nextScreen, transition: reveal)
        
        if let scene = GameScene(fileNamed:"GameScene") {
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            self.view!.presentScene(scene)
        }
        
    }
    
}