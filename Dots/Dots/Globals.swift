//
//  Globals.swift
//  Dots
//
//  Created by Jacob Ruzi on 5/1/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

import Foundation
import SpriteKit

let dotColors = [UIColor(red: 253/255.0, green: 227/255.0, blue: 167/255.0, alpha: 1.0), UIColor(red: 129/255.0, green: 207/255.0, blue: 224/255.0, alpha: 1.0), UIColor(red: 220/255.0, green: 198/255.0, blue: 224/255.0, alpha: 1.0), UIColor(red: 241/255.0, green: 169/255.0, blue: 160/255.0, alpha: 1.0), UIColor(red: 134/255.0, green: 226/255.0, blue: 213/255.0, alpha: 1.0), UIColor(red: 210/255.0, green: 215/255.0, blue: 211/255.0, alpha: 1.0), UIColor(red: 236/255.0, green: 100/255.0, blue: 75/255.0, alpha: 1.0), UIColor(red: 190/255.0, green: 144/255.0, blue: 212/255.0, alpha: 1.0), UIColor(red: 228/255.0, green: 241/255.0, blue: 254/255.0, alpha: 1.0), UIColor(red: 144/255.0, green: 198/255.0, blue: 149/255.0, alpha: 1.0), UIColor(red: 245/255.0, green: 215/255.0, blue: 110/255.0, alpha: 1.0)]