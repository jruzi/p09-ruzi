//
//  RulesScene.swift
//  Dots
//
//  Created by Jacob Ruzi on 5/1/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

import Foundation
import SpriteKit

class RulesScene: SKScene{
    
    //Contact bit masks
    let playerCategory: UInt32 = 0x1 << 0
    let otherCategory: UInt32 = 0x1 << 1

    override func didMoveToView(view: SKView) {
        self.backgroundColor = UIColor.whiteColor()
        
        //Set up rule 1
        let myLabel = SKLabelNode(fontNamed:"Avenir-Roman")
        myLabel.text = "tap to move"
        myLabel.fontSize = 40
        myLabel.fontColor = UIColor.blackColor()
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:0.90 * self.frame.size.height)
        myLabel.zPosition = 2

        //Set up player dot
        let playerDot = SKShapeNode(circleOfRadius: 11)
        playerDot.fillColor = UIColor.blackColor()
        playerDot.strokeColor = playerDot.fillColor
        playerDot.position = CGPoint(x: CGRectGetMidX(self.frame)-50, y: myLabel.position.y-25)
        
        let moveRight = SKAction.moveToX(CGRectGetMidX(self.frame)+50, duration: 2.0)
        let moveLeft = SKAction.moveToX(CGRectGetMidX(self.frame)-50, duration: 2.0)
        let move = SKAction.sequence([moveRight,moveLeft])
        let moving = SKAction.repeatActionForever(move)
        playerDot.runAction(moving)
        
        //Set up rule 2
        let myLabel2 = SKLabelNode(fontNamed:"Avenir-Roman")
        myLabel2.text = "absorb small dots"
        myLabel2.fontSize = 40
        myLabel2.fontColor = UIColor.blackColor()
        myLabel2.position = CGPoint(x:CGRectGetMidX(self.frame), y:0.55 * self.frame.size.height)
        myLabel2.zPosition = 2
        
        let playerDot2 = SKShapeNode(circleOfRadius: 11)
        playerDot2.fillColor = UIColor.blackColor()
        playerDot2.strokeColor = playerDot.fillColor
        playerDot2.position = CGPoint(x: CGRectGetMidX(self.frame)-50, y: myLabel2.position.y-25)
        
        let enemyDot = SKShapeNode(circleOfRadius: 7)
        enemyDot.fillColor = dotColors[Int(arc4random_uniform(UInt32(dotColors.count)))]
        enemyDot.strokeColor = enemyDot.fillColor
        enemyDot.position = CGPoint(x: CGRectGetMidX(self.frame)+50, y: playerDot2.position.y)
        
        let resetRule = SKAction.runBlock({playerDot2.position = CGPoint(x: CGRectGetMidX(self.frame)-50, y: myLabel2.position.y-25)})
        let rule2 = SKAction.sequence([moveRight, resetRule])
        let rule2repeat = SKAction.repeatActionForever(rule2)
        playerDot2.runAction(rule2repeat)
        
        //Set up rule 3
        let myLabel3 = SKLabelNode(fontNamed:"Avenir-Roman")
        myLabel3.text = "avoid big dots"
        myLabel3.fontSize = 40
        myLabel3.fontColor = UIColor.blackColor()
        myLabel3.position = CGPoint(x:CGRectGetMidX(self.frame), y:0.20 * self.frame.size.height)
        myLabel3.zPosition = 2
        
        let playerDot3 = SKShapeNode(circleOfRadius: 11)
        playerDot3.fillColor = UIColor.blackColor()
        playerDot3.strokeColor = playerDot3.fillColor
        playerDot3.position = CGPoint(x: CGRectGetMidX(self.frame)-50, y: myLabel3.position.y-25)
        
        let enemyDot2 = SKShapeNode(circleOfRadius: 15)
        enemyDot2.fillColor = dotColors[Int(arc4random_uniform(UInt32(dotColors.count)))]
        enemyDot2.strokeColor = enemyDot2.fillColor
        enemyDot2.position = CGPoint(x: CGRectGetMidX(self.frame)+50, y: playerDot3.position.y)
        
        let resetRule2 = SKAction.runBlock({enemyDot2.position = CGPoint(x: CGRectGetMidX(self.frame)+50, y: playerDot3.position.y)})
        let rule3 = SKAction.sequence([moveLeft,resetRule2])
        let rule3repeat = SKAction.repeatActionForever(rule3)
        enemyDot2.runAction(rule3repeat)
        
        self.addChild(myLabel)
        self.addChild(playerDot)
        self.addChild(myLabel2)
        self.addChild(enemyDot)
        self.addChild(playerDot2)
        self.addChild(myLabel3)
        self.addChild(playerDot3)
        self.addChild(enemyDot2)
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch ends */
        
        //Return to main menu
        if let scene = GameScene(fileNamed:"GameScene") {
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            self.view!.presentScene(scene)
        }
        
    }
    
}