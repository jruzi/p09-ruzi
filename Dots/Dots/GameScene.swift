//
//  GameScene.swift
//  Dots
//
//  Created by Jacob Ruzi on 4/29/16.
//  Copyright (c) 2016 Jacob Ruzi. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var numDots = 0
    
    let playTile = SKShapeNode()
    let ruleTile = SKShapeNode()
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.backgroundColor = UIColor.whiteColor()
        
        //Get the high score
        var highScore = NSKeyedUnarchiver.unarchiveObjectWithFile(GameData.ArchiveURL.path!) as? Int
        if highScore == nil {
            highScore = 0
        }
        
        //High score label
        let hscoreLabel = SKLabelNode(fontNamed: "Avenir-Book")
        hscoreLabel.text = String("High Score: " + String(highScore!))
        hscoreLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)/4)
        hscoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        hscoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        hscoreLabel.zPosition = -1
        hscoreLabel.alpha = 0.7
        hscoreLabel.fontSize = 40
        hscoreLabel.fontColor = UIColor(red: 108/255.0, green: 122/255.0, blue: 137/255.0, alpha: 0.6)
        
        //Game title label
        let myLabel = SKLabelNode(fontNamed:"SavoyeLetPlain")
        myLabel.text = "dots"
        myLabel.fontSize = 100
        myLabel.fontColor = UIColor.blackColor()
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:0.6 * self.frame.size.height)
        myLabel.zPosition = 2
        
        //Play game label
    
        playTile.alpha = 0.8
        playTile.path = CGPathCreateWithRoundedRect(CGRectMake(-150,0,300,70), 8, 8, nil)
        playTile.strokeColor = UIColor(red: 210/255.0, green: 215/255.0, blue: 211/255.0, alpha: 1.0)
        playTile.fillColor = playTile.strokeColor
        playTile.position = CGPoint(x:CGRectGetMidX(self.frame), y:myLabel.position.y - myLabel.frame.size.height - 13)
        playTile.zPosition = 1
        
        let playLabel = SKLabelNode(fontNamed:"SavoyeLetPlain")
        playLabel.text = "play game"
        playLabel.fontSize = 70
        playLabel.fontColor = UIColor.blackColor()
        playLabel.zPosition = 2
        playLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        playLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        playLabel.position = CGPoint(x:CGRectGetMidX(playTile.frame), y:CGRectGetMidY(playTile.frame))
        
        //Rules label
        
        ruleTile.alpha = 0.8
        ruleTile.path = CGPathCreateWithRoundedRect(CGRectMake(-150,0,300,70), 8, 8, nil)
        ruleTile.strokeColor = UIColor(red: 210/255.0, green: 215/255.0, blue: 211/255.0, alpha: 1.0)
        ruleTile.fillColor = playTile.strokeColor
        ruleTile.position = CGPoint(x:CGRectGetMidX(self.frame), y:playTile.position.y - playTile.frame.size.height - 13)
        ruleTile.zPosition = 1
        
        let ruleLabel = SKLabelNode(fontNamed:"SavoyeLetPlain")
        ruleLabel.text = "how to play"
        ruleLabel.fontSize = 70
        ruleLabel.fontColor = UIColor.blackColor()
        ruleLabel.zPosition = 2
        ruleLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        ruleLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        ruleLabel.position = CGPoint(x:CGRectGetMidX(ruleTile.frame), y:CGRectGetMidY(ruleTile.frame))
        

        
        //Have dots floating around in background
        if (numDots == 0) {
            for _ in numDots...50 {
                addDot()
            }
        }
        
        self.addChild(hscoreLabel)
        self.addChild(myLabel)
        self.addChild(playTile)
        self.addChild(playLabel)
        self.addChild(ruleTile)
        self.addChild(ruleLabel)
        
    }
    
    /* Adds dots for asthetics */
    func addDot() {
        numDots = numDots+1
        
        let dot = SKShapeNode(circleOfRadius: 8)
        dot.fillColor = dotColors[Int(arc4random_uniform(UInt32(dotColors.count)))]
        dot.strokeColor = dot.fillColor
        dot.position = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        dot.zPosition = 0
        
        let destination = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        
        let moveTo = SKAction.moveTo(destination, duration: 3.0 + NSTimeInterval(arc4random_uniform(6)))
        let fadeOut = SKAction.fadeOutWithDuration(1.0)
        let decCount = SKAction.runBlock({ self.numDots = self.numDots - 1})
        let sequence = SKAction.sequence([moveTo,fadeOut,decCount])
        
        dot.runAction(sequence)
        
        self.addChild(dot)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            //Play button is pushed
            if playTile.containsPoint(location) {
                //let reveal = SKTransition.fadeWithColor(UIColor.whiteColor(), duration: 1.0)
                let nextScreen = DotGame(size: self.view!.bounds.size)
                self.view!.presentScene(nextScreen)
            } else if ruleTile.containsPoint(location) {
                //let reveal = SKTransition.fadeWithColor(UIColor.whiteColor(), duration: 1.0)
                let nextScreen = RulesScene(size: self.view!.bounds.size)
                self.view!.presentScene(nextScreen)
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if numDots < 50 {
            addDot()
        }
    }
    
}
