//
//  GameData.swift
//  Dots
//
//  Created by Jacob Ruzi on 8/6/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

import Foundation

// inherit from NSCoding to make instances serializable
class GameData: NSObject, NSCoding {
    let score:Int;
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("scoredata")
    
    init(score:Int) {
        self.score = score;
    }
    
    required init(coder: NSCoder) {
        self.score = coder.decodeObjectForKey("score")! as! Int;
        super.init()
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.score, forKey: "score")
    }
}
