//
//  DotGame.swift
//  Dots
//
//  Created by Jacob Ruzi on 4/30/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

import Foundation
import SpriteKit

class DotGame: SKScene, SKPhysicsContactDelegate {
    
    //Contact bit masks
    let playerCategory: UInt32 = 0x1 << 0
    let otherCategory: UInt32 = 0x1 << 1
    
    var playerDot = SKShapeNode()
    
    var numDots = 0
    var dotsEaten = 0
    var doneGrowing = false
    
    let scoreLabel = SKLabelNode(fontNamed: "Avenir-Book")
 
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = UIColor.whiteColor()
        
        //Set up score label
        scoreLabel.text = String(dotsEaten)
        scoreLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        scoreLabel.zPosition = -1
        scoreLabel.alpha = 0.7
        scoreLabel.fontSize = 200
        scoreLabel.fontColor = UIColor(red: 236/255.0, green: 240/255.0, blue: 241/255.0, alpha: 1.0)
        
        //Set up player dot
        playerDot = SKShapeNode(circleOfRadius: 11)
        playerDot.fillColor = UIColor.blackColor()
        playerDot.strokeColor = playerDot.fillColor
        playerDot.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        playerDot.physicsBody = SKPhysicsBody(circleOfRadius:11)
        playerDot.physicsBody!.usesPreciseCollisionDetection = true
        playerDot.physicsBody!.categoryBitMask = playerCategory
        playerDot.physicsBody!.contactTestBitMask = otherCategory
        playerDot.physicsBody!.collisionBitMask = 0
        
        //Establish current view as physics delegate
        self.physicsBody = SKPhysicsBody(edgeFromPoint: CGPoint(x: 0, y: 0), toPoint: CGPoint(x: self.view!.frame.width,y: 0))
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsWorld.contactDelegate = self
        
        //Add some dots
        for _ in 0...15 {
            addDot()
        }
        
        //Add player dot to scene
        self.addChild(playerDot)
        self.addChild(scoreLabel)
        
    }
    
    func updateScore() {
        scoreLabel.text = String(dotsEaten)
    }
    
    func addDot() {
        numDots = numDots+1
        
        let dot = SKShapeNode(circleOfRadius: 1.0+CGFloat(arc4random_uniform(30)))
        dot.fillColor = dotColors[Int(arc4random_uniform(UInt32(dotColors.count)))]
        dot.strokeColor = dot.fillColor
        dot.zPosition = 0
        dot.name = "alive"
        
        //Dot's physics body
        dot.physicsBody = SKPhysicsBody(circleOfRadius: dot.frame.size.width/2)
        dot.physicsBody!.usesPreciseCollisionDetection = true
        dot.physicsBody!.categoryBitMask = otherCategory
        dot.physicsBody!.contactTestBitMask = playerCategory
        dot.physicsBody!.collisionBitMask = 0
        
        //Determine at which ends of the screen the dots appear and disappear
        let startWall = arc4random_uniform(4)
        var endWall = arc4random_uniform(4)
        while (startWall == endWall) {
            endWall = arc4random_uniform(4)
        }
        
        if startWall == 0 { //left wall
            dot.position = CGPoint(x:0, y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        } else if startWall == 1 { //right wall
            dot.position = CGPoint(x:self.frame.size.width, y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        } else if startWall == 2 { //top wall
            dot.position = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y: self.frame.size.height)
        } else { //bottom wall
            dot.position = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y: 0)
        }
        
        var destination = CGPoint(x:0, y:0)
        
        if endWall == 0 { //left wall
            destination = CGPoint(x:0, y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        } else if endWall == 1 { //right wall
            destination = CGPoint(x:self.frame.size.width, y:CGFloat(arc4random_uniform(UInt32(self.frame.size.height))))
        } else if endWall == 2 { //top wall
            destination = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y: self.frame.size.height)
        } else { //bottom wall
            destination = CGPoint(x:CGFloat(arc4random_uniform(UInt32(self.frame.size.width))), y: 0)
        }
        
        let moveTo = SKAction.moveTo(destination, duration: 7.0 + NSTimeInterval(arc4random_uniform(6)))
        let fadeOut = SKAction.fadeOutWithDuration(1.0)
        let decCount = SKAction.runBlock({ self.numDots = self.numDots - 1})
        let sequence = SKAction.sequence([moveTo,fadeOut,decCount])
        
        
        dot.runAction(sequence)
        
        self.addChild(dot)
    }
    
    func scaleUpPlayer(eatenSize : CGFloat) {
        if !doneGrowing {
            
            //Check if this is the last time the player should grow
            if playerDot.frame.size.width > 50 {
                doneGrowing = true
            }
            
            let grow = SKAction.scaleBy(1+eatenSize/1000, duration: 0.5)
            
            playerDot.runAction(grow)
            
            let saveVelocity = playerDot.physicsBody!.velocity
            
            //Remake physics body
            playerDot.physicsBody = SKPhysicsBody(circleOfRadius:playerDot.frame.size.width/2)
            playerDot.physicsBody!.usesPreciseCollisionDetection = true
            playerDot.physicsBody!.categoryBitMask = playerCategory
            playerDot.physicsBody!.contactTestBitMask = otherCategory
            playerDot.physicsBody!.collisionBitMask = 0
            playerDot.physicsBody!.velocity = saveVelocity
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            //Impulse is applied based on where user touched relative to the dot
            let deltaMovement = CGPointMake(location.x - playerDot.position.x, location.y - playerDot.position.y)
            let angle = atan(deltaMovement.y / deltaMovement.x)
            let distanceMovement = sqrt(pow(deltaMovement.x,2) + pow(deltaMovement.y,2))
            let degreeAngle = angle * CGFloat(180.0 / M_PI)
            print(degreeAngle)
            
            var switchXDir = CGFloat(1.0)
            var switchYDir = CGFloat(1.0)
            
            if degreeAngle >= 0 && degreeAngle <= 90 && deltaMovement.x > 0.0 {
                //Up right
                switchXDir = CGFloat(1.0)
                switchYDir = CGFloat(1.0)
            } else if degreeAngle >= -90 && degreeAngle <= 0 && deltaMovement.x < 0.0 {
                //Up left
                switchXDir = CGFloat(-1.0)
                switchYDir = CGFloat(-1.0)
            } else if degreeAngle >= 0 && degreeAngle <= 90 && deltaMovement.x < 0.0 {
                //Down left
                switchXDir = CGFloat(-1.0)
                switchYDir = CGFloat(-1.0)
            } else if degreeAngle >= -90 && degreeAngle <= 0 && deltaMovement.x > 0.0 {
                //Down right
                switchXDir = CGFloat(1.0)
                switchYDir = CGFloat(1.0)
            }
            
            playerDot.physicsBody!.applyImpulse(CGVectorMake(switchXDir * distanceMovement*cos(angle)/300.0, switchYDir * distanceMovement*sin(angle)/300.0))
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if playerDot.position.x < 0 {
            playerDot.position = CGPointMake(self.frame.size.width, playerDot.position.y)
        } else if playerDot.position.x > self.frame.size.width {
            playerDot.position = CGPointMake(0, playerDot.position.y)
        } else if playerDot.position.y < 0 {
            playerDot.position = CGPointMake(playerDot.position.x, self.frame.size.height)
        } else if playerDot.position.y > self.frame.size.height {
            playerDot.position = CGPointMake(playerDot.position.x, 0)
        }
        
        if numDots < 15 {
            addDot()
        }
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        print("Contact began")
        
        var firstBody = contact.bodyA
        var secondBody = contact.bodyB
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
        {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else
        {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody.categoryBitMask == playerCategory && secondBody.categoryBitMask == otherCategory && secondBody.node!.name == "alive") {
            if playerDot.frame.size.width > secondBody.node!.frame.size.width {
                secondBody.node!.name = "dead"
                
                let eatenDotSize = secondBody.node!.frame.size.width / 2
                
                let fade = SKAction.fadeOutWithDuration(0.5)
                let remove = SKAction.removeFromParent()
                let sequence = SKAction.sequence([fade,remove])
                secondBody.node!.runAction(sequence)
                
                numDots = numDots - 1
                dotsEaten = dotsEaten + 1
                
                scaleUpPlayer(eatenDotSize)
                updateScore()
            } else { //Player lost
                //let reveal = SKTransition.fadeWithColor(UIColor.whiteColor(), duration: 1.0)
                let nextScreen = GameOver(size: self.view!.bounds.size)
                nextScreen.score = dotsEaten
                self.view!.presentScene(nextScreen)
            }
            
        }
    }
    
}